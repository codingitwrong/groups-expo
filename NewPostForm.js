import React, {useState} from 'react';
import {Pressable, StyleSheet, Text, TextInput} from 'react-native';

export default function NewPostForm({onSubmit}) {
  const [message, setMessage] = useState('');

  const handleSubmit = () => {
    onSubmit(message).then(() => setMessage(''));
  };

  return (
    <>
      <TextInput
        value={message}
        onChangeText={setMessage}
        style={styles.textField}
      />
      <Pressable style={styles.button} onPress={handleSubmit}>
        <Text>Send</Text>
      </Pressable>
    </>
  );
}

const styles = StyleSheet.create({
  textField: {
    borderWidth: 1,
    borderColor: 'gray',
    padding: 5,
  },
  button: {
    borderWidth: 1,
    borderColor: 'gray',
    padding: 5,
  },
});
