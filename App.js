import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {SafeAreaView, StatusBar, Text} from 'react-native';
import NewPostForm from './NewPostForm';

const host = 'localhost:3000';
const httpUrl = `http://${host}`;
const wsUrl = `ws://${host}/ws`;
const groupId = 1;

const client = axios.create({
  baseURL: httpUrl,
  headers: {'Content-Type': 'application/json'},
});

const addPost = ({setPosts, post}) =>
  setPosts(posts => {
    if (posts.find(e => e.id === post.id)) {
      return posts;
    }
    return [post, ...posts];
  });

export default function App() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    client.get(`/groups/${groupId}/posts`).then(response => {
      const newPosts = [...response.data];
      newPosts.reverse();
      setPosts(newPosts);
    });
  }, []);

  useEffect(() => {
    const socket = new WebSocket(wsUrl);

    socket.addEventListener('open', () => {
      console.log('web socket connected');
    });

    socket.addEventListener('message', event => {
      const post = JSON.parse(event.data);
      addPost({setPosts, post});
    });

    return () => socket.close();
  }, []);

  function handleSubmit(message) {
    const postBody = {message};
    return client
      .post(`/groups/${groupId}/posts`, postBody)
      .then(response => addPost({setPosts, post: response.data}));
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <NewPostForm onSubmit={handleSubmit} />
        {posts.map(post => (
          <Text key={post.id}>{post.message}</Text>
        ))}
      </SafeAreaView>
    </>
  );
}
